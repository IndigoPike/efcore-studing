﻿using Samurai.Data;
using System;
using System.Linq;

namespace Samurai
{
    class Program
    {
        static SamuraiContext context = new SamuraiContext();

        static void Main(string[] args)
        {
            context.Database.EnsureCreated();
            GetSamurais("Before adding");
            AddSamurais(5);
            GetSamurais("After adding");
        }

        static void AddSamurais(int count)
        {
            for (int i = 0; i < count; i++)
            {
                var s = new Samurai.Domain.Samurai { Name = UI.Names.GetName() };
                context.Add(s);
            }
            context.SaveChanges();
        }

        static void AddSamurai(string name = "Jack")
        {
            var s = new Samurai.Domain.Samurai { Name = name };
            context.Add(s);
            context.SaveChanges();
        }

        static void GetSamurais(string message)
        {
            var samurais = context.Samurais.ToList();
            Console.WriteLine($"{message}: Samurais count {samurais.Count}");
            foreach(var s in samurais)
            {
                Console.WriteLine($"Samurai {s.Name} \t\t| id {s.Id}");
            }
        }
    }
}
